# donnees-extraites-assemblee

Données extraites de l'open data de l'Assemblée, afin d'étudier les amendements et les projets de lois de l'Assemblée, pour le projet [LexImpact](https://leximpact.an.fr/)

Les données extraites par ces scripts sont produites par les scripts du projet Git [etudes-amendements](https://github.com/leximpact/etudes-amendements).
